echo Welcome to Xfce panel fixer.
echo Press any key you want if you want to continue or press CTRL+C twice to exit.
curl -LO https://github.com/MatusModder/FriendlyOS-xfce-panel-fixer/releases/download/alpha/FriendlyOS.tar.bz2
xfce4-panel-profiles load FriendlyOS.tar.bz2
exit
